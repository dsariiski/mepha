// document.addEventListener("DOMContentLoaded", function (event) {
$(document).ready(function () {
    /* *selectors* */

    // jQuery selectors

    const requiredOptionSelector = $("select[required]");
    const optionalOptionSelector = $(".optional-select");

    /* header */

    var leftHeader = (document.getElementsByClassName("left-side-header") || [])[0];
    var rightHeader = (document.getElementsByClassName("right-side-header") || [])[0];
    var header = (document.getElementsByTagName('header') || [])[0];
    var mainContainer = (document.getElementsByClassName("main-container") || [])[1];

    /* form */
    const registerForm = getById("register-form");

    /* button */
    const registerBtn = getById("register-btn");

    /* input fields */
    const firstName = getById("vorname");
    const lastName = getById("nachname");
    const email = getById("email");
    const companyName = getById("unternehmensname");
    const street = getById("strasse");
    const houseNumber = getById("hausnummer");
    const postalCode = getById("postleitzahl");
    const city = getById("ort");

    /* messages */
    const validName = valid("name");

    /* !selectors */

    /* global-ish variables */
    var concealed = false;
    var hidden = false;
    var lastScrollPosition = 0;
    var scrollCount = 0;

    const messages = {
        firstName: valid("name"),
        lastname: valid("name"),
        email: valid("email"),
        companyName: valid("company name"),
        street: valid("street name"),
        houseNumber: valid("house number"),
        postalCode: valid("postal code"),
        city: valid("city")
    }

    /* regex */

    const regexLetter = new RegExp(/^[a-zA-Z ]$/);
    const regexDigit = new RegExp(/^[0-9 ]$/);
    const regexSpecial = new RegExp(/^[!@#$%^&*()_+\-=,.<>;':"\[\]\{\}\/\?\`~]$/);
    const regexEmailSpecial = new RegExp(/^[ !#$%^&*()_+\-=,<>;':"\[\]\{\}\/\?\`~]$/);
    const regexCitySpecial = new RegExp(/^[^a-zA-Z ]$/);

    const regexEmail = new RegExp(/^[\w.]+@[\w]+(\.[\w]{2,})+$/);

    const regexCity = new RegExp(/^([a-zA-Z]+)[ ]*([a-zA-Z]+[ ]*)*$/);

    const regexPostal = new RegExp(/^[0-9]{4}$/);
    const regexNumber = new RegExp(/^[0-9]{2,}$/);
    const regexCommon = new RegExp(/^[a-zA-Z]{2,}$/);

    const regexUncommonlyCommon = new RegExp(/^.*\S.*$/);

    const regexHouseNumber = new RegExp(/^[0-9]{1,}$/)

    const inputs = {
        firstName: firstName,
        lastName: lastName,
        email: email,
        companyName: companyName,
        street: street,
        houseNumber: houseNumber,
        postalCode: postalCode,
        city: city
    };

    resizeHandler();

    const mobileLanguagePlaceholder = $(".mobile-language-placeholder");

    var isValidField = false;

    for (let input in inputs) {
        (function () {
            var currentInput = inputs[input];
            const currentId = input;

            /* focus events */

            currentInput.addEventListener("focus", function (event) {
                const label = event.target.previousElementSibling;

                label.classList.add("label-active");
                this.classList.add("input-active");

            })

            currentInput.addEventListener("blur", function (event) {
                const label = event.target.previousElementSibling;
                const labelTitleArray = label.textContent.split(" ");
                labelTitle = labelTitleArray[labelTitleArray.length - 1];

                if (event.target.value.length == 0) {

                    label.classList.remove("label-active");
                    this.classList.remove("input-active");


                    label.textContent = labelTitle;
                }
            })

            /* validation events */

            currentInput.addEventListener("keydown", function (event) {
                const keyPressed = (event.key || "").toLowerCase();

                var isValid = false;

                switch (currentId) {
                    case "email":
                        isValid = regexEmailSpecial.test(keyPressed) ? false : true;

                        if (isValid) {
                            return;
                        } else {
                            return event.preventDefault();
                        }
                        break;
                    case "houseNumber":
                        isValid = regexLetter.test(keyPressed) ? false : true;
                        break;
                    case "postalCode":
                        isValid = regexLetter.test(keyPressed) ? false : true;

                        if (event.target.value.length === 4 && keyPressed.length === 1) {
                            return event.preventDefault();
                        }
                        break;
                    case "city":
                        isValid = regexCitySpecial.test(keyPressed) ? false : true;

                        if (!isValid) {
                            return event.preventDefault();
                        } else {
                            return;
                        }
                        break;
                    case "companyName":
                    case "street":
                        return;
                        break;
                    default:
                        isValid = regexDigit.test(keyPressed) ? false : true;
                        break;
                }

                if (keyPressed === "spacebar" || regexSpecial.test(keyPressed)) {
                    return event.preventDefault();
                }

                if (!isValid) {
                    return event.preventDefault();
                }
            })

            currentInput.addEventListener("keyup", function (event) {
                const currentValue = event.target.value;
                const currentLetter = currentValue.substring(currentValue.length - 1);

                const label = event.target.previousElementSibling;

                const labelTitle = label.textContent.charAt(0).toUpperCase() + label.textContent.slice(1);
                const labelTitleArray = label.textContent.split(" ")

                isValidField = false;

                /* validation */
                isValidField = regexUncommonlyCommon.test(currentValue);

                if (currentId === "email") {
                    isValidField = regexEmail.test(currentValue);
                } else if (currentId === "houseNumber") {
                    isValidField = regexNumber.test(currentValue);
                } else if (currentId === "postalCode") {
                    isValidField = regexPostal.test(currentValue);
                } else if (currentId === "city") {
                    isValidField = regexCity.test(currentValue);
                } else if (currentId === "street") {
                    if (isValidField) {
                        return;
                    }
                } else if (currentId === "companyName") {
                    if (isValidField) {
                        return;
                    }
                } else {
                    isValidField = regexCommon.test(currentValue);
                }

                /* styles */

                if (isValidField || currentValue.length === 0) {
                    label.textContent = labelTitleArray[labelTitleArray.length - 1];

                    label.classList.remove("label-invalid");
                    this.classList.remove("input-invalid");
                } else {
                    label.textContent = valid(labelTitleArray[labelTitleArray.length - 1]);

                    this.classList.remove("input-active");

                    label.classList.add('label-invalid');
                    this.classList.add("input-invalid");

                }
            })
        }())
    }

    var dataRequired = [
        {
            id: "option-1",
            text: "Option 1"
        },
        {
            id: "option-2",
            text: "Option 2"
        }
    ]

    var dataOptional = [
        {
            id: "option-3",
            text: "Option 3"
        },
        {
            id: "option-4",
            text: "Option 4"
        }
    ]

    const selectConfigRequired = {
        placeholder: "Select an option",
        width: "style",
        theme: "select-slick-required",
        data: dataRequired
    }

    const selectConfigOptional = {
        placeholder: "Optionowl",
        width: "style",
        theme: "select-slick",
        data: dataOptional
    }

    const selectConfigLanguage = { 
        placeholder: "DE",
        // data:languageDropdownData,
        width:"style",
        theme: "select-mobile"
    }

    requiredOptionSelector.select2(selectConfigRequired);
    optionalOptionSelector.select2(selectConfigOptional);

    const select2Selects = [requiredOptionSelector, optionalOptionSelector];
    const selectsRendered = $(".select2-container");

    /*
    selectsRendered.click(function(event){
        event.stopPropagation();

        // this.classList.toggle("select2-active");
    })

    selectsRendered.focusout(function (event) {
        event.stopPropagation();

        // this.classList.remove("select2-active");
    })

    for (var select in select2Selects) {
        select2Selects[select].on("select2:select", function (event) {
            registerForm.click();
        })
    }

    /*
    function validateForm(event) {
        TODO
    }
    */

    registerForm.addEventListener("keyup", formActivityHandler);
    registerForm.addEventListener("click", formActivityHandler);

    function formActivityHandler(event) {
        event.stopPropagation();

        // console.log(requiredOptionSelector);

        const requiredFields = registerForm.querySelectorAll(":required");

        var canRegister = true;

        for (var fieldWrapper in requiredFields) {
            (function () {
                var field = requiredFields[fieldWrapper];

                // console.dir(field);

                if (!field.type) {
                    return;
                }

                // console.log("field.value: " + field.value);

                if (field.type.toLowerCase() === "checkbox" && !field.checked) {
                    canRegister = false;
                } else if (!regexUncommonlyCommon.test(field.value)) {
                    canRegister = false;
                }
            })()
        }

        var btnBackgroundColor;

        if (canRegister) {
            registerBtn.classList.add("button-active");
        } else {
            registerBtn.classList.remove("button-active");
        }

        registerBtn.style["background-color"] = btnBackgroundColor;
    }

    function scrollHandler(event) {
        if (!header.className.includes('desktop')) {
            return;
        }
        const minScroll = 30;
        const maxScroll = 1000;

        leftHeader = document.getElementsByClassName("left-side-header")[0];
        header = document.getElementsByTagName('header')[0];

        const currentScrollPosition = Math.round(window.pageYOffset);

        const diff = currentScrollPosition - lastScrollPosition;

        if (currentScrollPosition < 45) {
            leftHeader.classList.remove("left-side-header-sticky");
            header.classList.remove("header-hidden");
            header.classList.remove("header-sticky");
            hidden = false;
            concealed = false;
        } else if (currentScrollPosition > 45 && currentScrollPosition < 500) {
            header.classList.remove("header-hidden");
            header.classList.add("header-sticky");
            leftHeader.classList.add("left-side-header-sticky");
            concealed = true;
            hidden = false;
        } else {
            header.classList.add("header-hidden");
            concealed = false;
            hidden = true;
        }

        lastScrollPosition = currentScrollPosition;
    }

    window.addEventListener("scroll", scrollHandler)

    window.addEventListener("mousemove", function (event) {
        if (!header.className.includes('desktop')) {
            return;
        }
        const positionCursorYCurrent = event.clientY;

        if (positionCursorYCurrent < 90) {
            leftHeader.classList.remove("left-side-header-sticky");
            header.classList.remove("header-hidden");
            header.classList.remove("header-sticky");
        } else {
            if (concealed) {
                header.classList.add("header-sticky");
                leftHeader.classList.add("left-side-header-sticky");
            } else if (hidden) {
                header.classList.remove("header-sticky");
                header.classList.add("header-hidden");
            }
        }
    })

    function resizeHandler(event) {
        const currentWidth = window.innerWidth;
        const headerSelector = $('header');

        if (currentWidth >= 1024 && !headerSelector.hasClass('desktop')) {
            activate("desktop");
            rebuildHeaderSelectors();
            attachEventHandlers("desktop");
        } else if (currentWidth < 1024 && !headerSelector.hasClass('mobile')) {
            activate("mobile");
            rebuildHeaderSelectors();
            attachEventHandlers("mobile");
        }
    }

    window.onresize = resizeHandler;

    function rebuildHeaderSelectors() {
        leftHeader = (document.getElementsByClassName("left-side-header") || [])[0];
        rightHeader = (document.getElementsByClassName("right-side-header") || [])[0];
        header = document.getElementsByTagName('header')[0];
    }

    function attachEventHandlers(type) {
        if (type === "mobile") {
            const mobileMenu = $('.mobile-menu');
            const mainContainer = $('.main-container');
            const footer = $('footer');
            const hamburgerMenu = $('.hamburger-menu');

            const midLine = hamburgerMenu.find(".middle-line")[0];
            const upperLine = hamburgerMenu.find('.upper-line')[0];
            const lowerLine = hamburgerMenu.find('.lower-line')[0];

            hamburgerMenu.click(function (event) {
                const fading = midLine.className.includes('fading') ? true : false;

                if (fading) {
                    midLine.classList.remove('fading');
                    upperLine.classList.remove('rotate-this-way');
                    lowerLine.classList.remove('rotate-that-way');

                    mobileMenu.removeClass('menu-show');

                    mobileLanguagePlaceholder.hide();
                    mobileLanguagePlaceholder.select2('destroy');
                } else {
                    midLine.classList.add('fading');
                    upperLine.classList.add('rotate-this-way');
                    lowerLine.classList.add('rotate-that-way');

                    mobileMenu.addClass('menu-show');
                    
                    setTimeout(() => {
                        mobileLanguagePlaceholder.select2(selectConfigLanguage);
                    }, 200);
                    
                }

                /*
                $('.mobile-language-placeholder').select2(
                    { 
                        placeholder: "DE",
                        // data:languageDropdownData,
                        width:"style",
                        theme: "select-mobile"
                    }
                );
                */
            })

            mobileMenu.click(showSubNav);
        } else {
            rightHeader.addEventListener("click", showSubNav);
        }
    }

    /* event handlers */
    function showSubNav(event) {
        const isLi = event.target.tagName.toLowerCase();
        const isParent = event.target.className.toLowerCase() === "list-parent" ? true : false;

        if (isLi && isParent) {
            const menuParents = document.getElementsByClassName("sub-nav-links");

            for (var menuParent in menuParents) {
                
                if((menuParents[menuParent].className || "").includes("hidden")){
                    continue;
                }

                menuParents[menuParent].className = menuParents[menuParent].className + " hidden";
            }

            const children = event.target.children;
            var subList = children[0];

            if (children.length > 1) {
                subList = children[1];
            }

            if (subList.classList.length > 1) {
                subList.classList.remove("hidden");
            }
        }
    }
    /* event handlers */
})
