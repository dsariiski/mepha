const regexNumber = new RegExp(/^[0-9]$/);
const regexCommon = new RegExp(/^[a-zA-Z]+$/);

function getById(elementId) {
    return document.getElementById(elementId);
}

/* valid message generator */
function valid(fieldName) {
    return "Please enter a valid ".concat(fieldName);
}

/* checks if symbol is valid */
function isValidSymbol(symbol, type) {
    if (type === "number") {
        return regexNumber.test(symbol);
    }

    return regexCommon.test(symbol);
}

/* deletes invalid symbol */
function deleteInvalidSymbol(symbolType, event) {
    const word = event.target.value;
    const symbol = word.substring(word.length - 1)

    var isValid = false;

    if (symbolType === "number") {
        isValid = regexCommon.test(symbol);
    } else {
        isValid = regexNumber.test(symbol);
    }

    if (!isValid) {
        return event.target.value = word.substring(0, word.length - 1);
    }
}

function pxMaker(val) {
    val = Math.ceil(val);

    return val + "px";
}

/* templates */
function activate(type) {
    const oldHeader = $('header');
    const body = $('body');

    oldHeader.remove();

    /* new elements */
    const header = $("<header></header>");
    const logo = $('<img src="./resources/mepha-logo.svg" class="logo" alt="mepha-logo" />');
    const languageDropdown = $('<select></select>').addClass("language-placeholder");
    const deOption = $('<option>DE</option>');
    const frOption = $('<option>FR</option>');

    languageDropdown.append([deOption, frOption]);

    /* data */
    const liParentData1 = ["Wissen", "Rezeptfreie Medikamente", "Rezeptpflichtige Medikamente", "Service"];
    const liParentData2 = ["Rezeptfreie Medikamente", "Wissen", "Rezeptpflichtige Medikamente", "Service"];
    const liParentData3 = ["Service", "Wissen", "Rezeptpflichtige Medikamente", "Rezeptfreie Medikamente"];
    const liParentData4 = ["Rezeptpflichtige Medikamente", "Rezeptfreie Medikamente", "Wissen", "Service"];

    const hidden = true;
    const locked = true;

    if (type === "mobile") {
        const hamburgerMenu = $('<div class="hamburger-menu"></div>')

        const hamburgerLineClasses = ["upper-line", "middle-line", "lower-line"];

        for (var i = 0; i < 3; i++) {
            const hamburgerLine = tagMaker('span');
            hamburgerLine.addClass('hamburger-line');
            hamburgerLine.addClass(hamburgerLineClasses[i]);
            hamburgerMenu.append(hamburgerLine);
        }

        header.append(logo);
        header.addClass('mobile');

        body.prepend(hamburgerMenu);
        body.prepend(header);

        mobileMenuMaker();
    } else {
        const mobileMenu = $('.mobile-menu');
        const hamburgerMenu = $('.hamburger-menu')
        if (mobileMenu[0]) {
            mobileMenu.remove();
        }
        if (hamburgerMenu[0]) {
            hamburgerMenu.remove();
        }
        /*new elements */
        const leftSide = $('<div class="left-side-header"></div>');
        const rightSide = $('<div class="right-side-header"></div>');

        leftSide.append([logo, languageDropdown]);

        const mainContainer = $('<div></div>').addClass('main-header-container');
        const navLinks = $('<nav></nav>').addClass('nav-links');

        const liParent1 = liParentMaker("Gesundheitsportal", liParentData1);
        const liParent2 = liParentMaker("Unternehmen", liParentData2, hidden);
        const liParent3 = liParentMaker("Fachpersonen", liParentData3, hidden, locked);
        const liParent4 = liParentMaker("Shop", liParentData4, hidden, locked);

        const linkSectionContainer = $('<ul></ul>').addClass('link-section-container');

        linkSectionContainer.append([liParent1, liParent2, liParent3, liParent4]);
        navLinks.append(linkSectionContainer);
        mainContainer.append(navLinks);

        var userPanelSection = $('<section></section>').addClass('user-panel');

        const login = true;

        const userPanel = userPanelMaker(login);

        userPanelSection.append(userPanel);

        mainContainer.append(userPanelSection);
        rightSide.append(mainContainer);

        header.append([leftSide, rightSide]);
        header.addClass('desktop');

        body.prepend(header);
    }

    function mobileMenuMaker(username) {
        const menuExists = $('mobile-menu')[0] ? true : false;

        if (menuExists) {
            return;
        }

        const mobileMenu = tagMaker('aside');
        mobileMenu.addClass('mobile-menu');

        const mobileMenuContainer = tagMaker('section');
        mobileMenuContainer.addClass('mobile-menu-container');

        const languagePlaceholder = tagMaker('select');
        languagePlaceholder.addClass('mobile-language-placeholder');

        const languageDe = tagMaker('option', 'DE');
        const languageFr = tagMaker('option', 'FR');

        languagePlaceholder.append([languageDe, languageFr]);
        mobileMenuContainer.append(languagePlaceholder);

        const navParent = tagMaker('nav');
        const ulParent = tagMaker('ul');
        ulParent.addClass('ul-parent');

        const liParent1 = liParentMaker("Gesundheitsportal", liParentData1);
        const liParent2 = liParentMaker("Unternehmen", liParentData2, hidden);
        const liParent3 = liParentMaker("Fachpersonen", liParentData3, hidden, locked);
        const liParent4 = liParentMaker("Shop", liParentData4, hidden, locked);

        // const liParents = [liParent1, liParent2, liParent3, liParent4];

        // for(var liParent of liParents) {
        //     liParents[liParent].classList.add("mobile-list");
        // }

        ulParent.append([liParent1, liParent2, liParent3, liParent4]);
        navParent.append(ulParent);
        mobileMenuContainer.append(navParent);

        const login = false;
        const mobile = { username: "petkan draganov" };
        const userPanel = userPanelMaker(login, mobile);

        mobileMenu.append(userPanel);

        mobileMenu.append(mobileMenuContainer);

        body.prepend(mobileMenu);

        /*
        var languageDropdownData = [
            {
                id: "de-option",
                text: "DE"
            },
            {
                id: "fr-option",
                text: "FR"
            }
        ]
        */
    }

    function userPanelMaker(login, mobile) {
        const authNav = $('<nav></nav').addClass('auth');

        if (login) {
            const login = $('<a href="#">Login</a>');

            const lockLink = $('<a href="#"></a>');
            const lockIcon = $('<i class="fas fa-lock"></i>');

            lockLink.append(lockIcon);

            const doctorCheckLink = $('<a href="#"></a>');
            const doctorCheckIcon = $('<img src="./resources/doccheck.svg" />');

            doctorCheckLink.append(doctorCheckIcon);

            const rxLink = $('<a href="#">RX</a>');

            authNav.append([login, lockLink, doctorCheckLink, rxLink]);
        }

        const navBtnSearch = $('<a class="nav-button search icon-search" href="#"><i class="fas fa-search"></i></a>');
        const navBtnCart = $('<a class="nav-button search icon-shopping-cart" href="#"><i class="fas fa-shopping-cart"></i></a>');

        if (mobile) {
            const actions = $('<section class="actions"></section>');
            actions.addClass("actions");

            actions.append([navBtnSearch, navBtnCart]);

            const userInfo = $('<span></span>');
            userInfo.text(mobile.username);
            userInfo.addClass('user-info');

            authNav.append(actions);
            authNav.append(userInfo);
        } else {
            authNav.append([navBtnSearch, navBtnCart]);
        }

        return authNav;
        /*
        userPanelSection.append(authNav);

        return userPanelSection;
        */

    }

    function liParentMaker(title, ulData, hidden, locked) {
        title = " " + title;
        const li = tagMaker('li', title).addClass('list-parent');

        if (locked) {
            const lock = tagMaker('i', "&nbsp;").addClass('fas fa-lock');

            li.prepend(lock);
        }

        var ulClass = 'sub-nav-links';

        if (hidden) {
            ulClass += ' hidden';
        }

        const ulChild = ulMaker(ulData, ulClass);

        li.append(ulChild);

        return li;
    }

    function ulMaker(data, className, hidden) {
        const ul = tagMaker('ul').addClass(className);

        for (var ulChild in data) {
            const liRow = tagMaker('li', '')
            const anchor = $('<a href="#">' + data[ulChild] + '</a>');

            liRow.append(anchor)
            ul.append(liRow);
        }

        return ul;
    }
}

function tagMaker(tagName, data) {
    const tag = $('<' + tagName + '>' + (data || '') + '</' + tagName + '>');

    return tag;
}

/*
const listeners = {

}

const stylishValidation = {
    invalidText: function (element) {
        element.style.color
    }
}
*/
